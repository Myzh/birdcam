import picamera
import picamera.array
import argparse
import warnings
import datetime
import os
import json
import time
import cv2
import webdav3.client

# Parse the command line arguments.
argument_parser = argparse.ArgumentParser()
argument_parser.add_argument("-c", "--config", required=True, help="path to the JSON configuration file")
arguments = vars(argument_parser.parse_args())

# Filter warnings.
warnings.filterwarnings("ignore")

# Load the configuration.
config = json.load(open(arguments["config"]))

# Initialize the camera.
camera = picamera.PiCamera()
camera.resolution = config["resolution"]
camera.framerate = config["fps"]
raw_capture = picamera.array.PiRGBArray(camera, size=tuple(config["resolution"]))
print("[INFO] Camera stream initialized.")

# Log into the webdav account specified if enabled.
webdav_client = None
local_directory = "./.tmp/"
if config["enable_webdav"]:
  # Connect to the webdav server.
  options = {
    'webdav_hostname': config["webdav_server"],
    'webdav_login': config["webdav_user"],
    'webdav_password': config["webdav_password"]
  }
  webdav_client = webdav3.client.Client(options)
  # Create the remote path.
  temporary_path = ""
  for directory in config["webdav_path"].split("/"):
    temporary_path += "/" + directory
    #webdav_client.mkdir(temporary_path)
  # Create the local path for temporary files.
  os.makedirs(local_directory, exist_ok=True)
  print("[INFO] webdav account linked")

# Warmup the camera.
time.sleep(config["camera_warmup_time"])
print("[INFO] Camera is warmed up.")

# Initialize loop persistant variables.
average_model = None
last_uploaded = datetime.datetime.now()
upload_queued_frame = None
motion_counter = 0

# Capture frames from the camera.
for f in camera.capture_continuous(raw_capture, format="bgr", use_video_port=True):
  # Get the frame and current timestamp.
  frame = f.array
  timestamp = datetime.datetime.now()

  # Initialize the object detected flag.
  object_detected = False

  # Pre-process the frame
  processed_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
  processed_frame = cv2.GaussianBlur(processed_frame, (21, 21), 0)

  # Initialize the average model during the first iteration.
  if average_model is None:
    average_model = processed_frame.copy().astype("float")
    raw_capture.truncate(0)
    continue

  # Accumulate the average model with the current pre-processed frame and calculate the delta
  cv2.accumulateWeighted(processed_frame, average_model, 0.5)
  delta_frame = cv2.absdiff(processed_frame, cv2.convertScaleAbs(average_model))

  # Threshold the delta frame and find contours.
  threshold_frame = cv2.threshold(delta_frame, config["delta_threshold"], 255, cv2.THRESH_BINARY)[1]
  threshold_frame = cv2.dilate(threshold_frame, None, iterations=2)
  contours = cv2.findContours(threshold_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

  # Use different tuple entry depending on the OpenCV version.
  if len(contours) == 2:
    contours = contours[0]
  elif len(contours) == 3:
    contours = contours[1]

  # Loop over the contours.
  for c in contours:
    # Ignore small contours.
    if cv2.contourArea(c) >= config["min_area"]:
      object_detected = True
      break

  if object_detected:
    # Log the branch entry. 
    print("[INFO] Object detected.")

    # Check if enough time has passed since the last upload.
    if (timestamp - last_uploaded).seconds >= config["min_upload_time"]:
      # Increment the motion counter.
      motion_counter += 1

      # Only upload if there have been multiple consecutive motion frames to avoid false positives.
      if motion_counter >= config["min_motion_frames"]:
        # Check if there is a frame queued for upload.
        if upload_queued_frame is not None:
          # Check to see if webdav sohuld be used.
          if config["enable_webdav"]:
            class Filenames:
              def __init__(self, timestamp):
                self.base = timestamp.strftime("%s.jpg")
                self.local = local_directory + self.base
                self.remote = config["webdav_path"] + "/" + self.base

              # Callback function to handle a completed upload.
              def upload_callback(self):
                # Remove the local file if the upload was successful.
                if webdav_client.check(self.remote):
                  os.remove(self.local)
                else:
                  print("[ERROR] Failed to upload {}".format(self.base))

            # Generate filenames.
            filenames = Filenames(timestamp)
            # Write the image to a temporary file.
            cv2.imwrite(filenames.local, upload_queued_frame)
            
            # Upload the image to the webdav server and cleanup.
            webdav_client.upload_async(remote_path = filenames.remote, local_path = filenames.local, callback = filenames.upload_callback)
            print("[UPLOAD] {}".format(filenames.base))

        # Update the last uploaded timestamp and queue the current frame for upload.
        last_uploaded = timestamp
        upload_queued_frame = frame
  else:
    # Reset the motion counter and discard the last frame queued for upload.
    motion_counter = 0
    upload_queued_frame = None

  # clear the stream in preparation for the next frame
  raw_capture.truncate(0)
